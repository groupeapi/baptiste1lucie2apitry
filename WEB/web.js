var express = require('express')
const axios = require('axios')
var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({ extended: false })
var app = express();

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/'))

app.get('/form', function (req, res) {
    res.sendfile('./form.html');
});


app.post('/form', urlencodedParser, function (req, res) {//pick up data from form 
    res.sendfile('./form.html'); // send reponse

    axios.post('http://localhost:8080/user/new', req.body)//sent data tu API (sent form req.body)
        .then(function (res) {
            console.log('OK');
        })
        .catch(function (error) {
            console.log(error);
        });
});

app.get('/table', function (req, res) { // call site table
    
    axios.get('http://localhost:8080/user')//call data json from API
    .then(function (resu) {
        console.log(resu)
        res.render('table', { 'users': resu.data });
    })
    .catch(function (error) {
        console.log(error);
    });
    
});


app.get('/table/:id', function (req, res) {
    let id = req.params.id;
    axios.delete("http://localhost:8080/table/" + id)
    .then(function (resp) {
        res.redirect("http://localhost:8082/table")
        console.log(resp)
        res.status(200).send(); 
    })
    .catch(function (err) {
        res.status(404).send();  

    });
});

app.listen(8082);

















