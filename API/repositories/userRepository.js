const fs = require('fs');
const filename = 'newUser.json';
var {addUser, addUserWhenFileIsEmpty} = require('../helpers/userHelper');

module.exports = 
{
    insertUser: function (req) {
        fs.exists(filename, function (exists) {
            if (exists) { 
                return addUser(req);           
            }
            else {
                return addUserWhenFileIsEmpty(req);
            }

            
        })
    }
}

