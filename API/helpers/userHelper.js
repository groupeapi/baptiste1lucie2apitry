const fs = require('fs');
const uuidv1 = require('uuid/v1');
const filename = 'newUser.json';

module.exports = {
    addUser: function(req){
        fs.readFile(filename, function (err, data) { //if fs exists read my file and call callback function
            users = JSON.parse(data);

            let myJson = req.body; // add id to json
            myJson.id = uuidv1();
            users.push(myJson);
            let users_string = JSON.stringify(users, null, 2);

            fs.writeFile(filename, users_string, (err) => {
                if (err) {
                    return true;
                };
            });
        })
        return false;
    },

    addUserWhenFileIsEmpty: function(req) {
        users = [];
        let myJson = uuidv1().req.body; // add id to json
        users.push(myJson);
        let users_string = JSON.stringify(users, null, 2);

        fs.writeFile(filename, users_string, (err) => {
            if (err) return true;
        });
        return false;
    }

}